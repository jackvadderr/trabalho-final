CREATE USER user WITH ENCRYPTED PASSWORD '1234';
ALTER ROLE user SET client_encoding TO 'utf8';
ALTER ROLE user SET default_transaction_isolation TO 'read committed';
GRANT ALL PRIVILEGES ON DATABASE gestao_escolar TO user;
