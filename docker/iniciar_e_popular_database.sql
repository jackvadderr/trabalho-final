CREATE DATABASE IF NOT EXISTS gestao_escolar;
USE gestao_escolar;

CREATE TABLE IF NOT EXISTS Endereco (
  id bigint AUTO_INCREMENT,
  descricao varchar(200),
  logradouro varchar(200),
  data date,
  PRIMARY KEY (id)
);
CREATE TABLE IF NOT EXISTS Disciplina (
  id int AUTO_INCREMENT,
  descricao varchar(200),
  curso varchar(200),
  PRIMARY KEY (id)
);
CREATE TABLE IF NOT EXISTS Aluno (
  id int AUTO_INCREMENT,
  nome varchar(200),
  sobrenome varchar(200),
  dataNascimento Date,
  curso varchar(200),
  PRIMARY KEY (id)
);
CREATE TABLE IF NOT EXISTS Matricula (
  disciplina int,
  aluno int,
  periodo varchar(200),
  PRIMARY KEY (periodo, disciplina, aluno)
);
CREATE TABLE IF NOT EXISTS Professor (
  id int AUTO_INCREMENT,
  nome varchar(200),
  sobrenome varchar(200),
  dataNascimento Date,
  curso varchar(200),
  PRIMARY KEY(id)
);

INSERT INTO Aluno (id, nome, sobrenome, dataNascimento, curso) VALUES (1, 'João', 'Silva', '1990-01-01', 'SISTEMA');
INSERT INTO Aluno (id, nome, sobrenome, dataNascimento, curso) VALUES (2, 'Maria', 'Santos', '1992-02-15', 'DIREITO');
INSERT INTO Aluno (id, nome, sobrenome, dataNascimento, curso) VALUES (3, 'Pedro', 'Ferreira', '1991-03-20', 'SISTEMA');
INSERT INTO Aluno (id, nome, sobrenome, dataNascimento, curso) VALUES (4, 'Ana', 'Oliveira', '1993-04-10', 'DIREITO');
INSERT INTO Aluno (id, nome, sobrenome, dataNascimento, curso) VALUES (5, 'Lucas', 'Rodrigues', '1995-05-05', 'SISTEMA');
INSERT INTO Aluno (id, nome, sobrenome, dataNascimento, curso) VALUES (6, 'Carla', 'Ribeiro', '1994-06-30', 'DIREITO');
INSERT INTO Aluno (id, nome, sobrenome, dataNascimento, curso) VALUES (7, 'Gustavo', 'Lima', '1992-07-12', 'SISTEMA');
INSERT INTO Aluno (id, nome, sobrenome, dataNascimento, curso) VALUES (8, 'Fernanda', 'Alves', '1993-08-25', 'DIREITO');
INSERT INTO Aluno (id, nome, sobrenome, dataNascimento, curso) VALUES (9, 'Rafael', 'Pereira', '1991-09-18', 'SISTEMA');
INSERT INTO Aluno (id, nome, sobrenome, dataNascimento, curso) VALUES (10, 'Patrícia', 'Gomes', '1990-10-08', 'DIREITO');
INSERT INTO Aluno (id, nome, sobrenome, dataNascimento, curso) VALUES (11, 'Marcelo', 'Costa', '1994-11-26', 'SISTEMA');
INSERT INTO Aluno (id, nome, sobrenome, dataNascimento, curso) VALUES (12, 'Amanda', 'Martins', '1992-12-05', 'DIREITO');
INSERT INTO Aluno (id, nome, sobrenome, dataNascimento, curso) VALUES (13, 'Bruno', 'Sousa', '1993-02-22', 'SISTEMA');
INSERT INTO Aluno (id, nome, sobrenome, dataNascimento, curso) VALUES (14, 'Laura', 'Carvalho', '1991-03-17', 'DIREITO');
INSERT INTO Aluno (id, nome, sobrenome, dataNascimento, curso) VALUES (15, 'Diego', 'Barbosa', '1990-04-14', 'SISTEMA');
INSERT INTO Aluno (id, nome, sobrenome, dataNascimento, curso) VALUES (16, 'Camila', 'Lopes', '1995-06-29', 'DIREITO');
INSERT INTO Aluno (id, nome, sobrenome, dataNascimento, curso) VALUES (17, 'Ricardo', 'Fernandes', '1994-07-08', 'SISTEMA');
INSERT INTO Aluno (id, nome, sobrenome, dataNascimento, curso) VALUES (18, 'Mariana', 'Cardoso', '1992-08-11', 'DIREITO');
INSERT INTO Aluno (id, nome, sobrenome, dataNascimento, curso) VALUES (19, 'André', 'Nunes', '1993-09-24', 'SISTEMA');
INSERT INTO Aluno (id, nome, sobrenome, dataNascimento, curso) VALUES (20, 'Juliana', 'Mendes', '1991-10-13', 'DIREITO');
INSERT INTO Aluno (id, nome, sobrenome, dataNascimento, curso) VALUES (21, 'Hugo', 'Pinto', '1990-11-09', 'SISTEMA');
INSERT INTO Aluno (id, nome, sobrenome, dataNascimento, curso) VALUES (22, 'Carolina', 'Freitas', '1992-12-27', 'DIREITO');
INSERT INTO Aluno (id, nome, sobrenome, dataNascimento, curso) VALUES (23, 'Renato', 'Araújo', '1994-01-04', 'SISTEMA');
INSERT INTO Aluno (id, nome, sobrenome, dataNascimento, curso) VALUES (24, 'Vitor', 'Siqueira', '1991-03-15', 'SISTEMA');
INSERT INTO Aluno (id, nome, sobrenome, dataNascimento, curso) VALUES (25, 'Luana', 'Gonçalves', '1995-04-19', 'DIREITO');

INSERT INTO Professor (id, nome, sobrenome, dataNascimento, curso) VALUES (1, 'Fernando', 'Silva', '1975-01-15', 'SISTEMA');
INSERT INTO Professor (id, nome, sobrenome, dataNascimento, curso) VALUES (2, 'Carolina', 'Ribeiro', '1980-03-02', 'DIREITO');
INSERT INTO Professor (id, nome, sobrenome, dataNascimento, curso) VALUES (3, 'Ricardo', 'Oliveira', '1978-05-20', 'SISTEMA');
INSERT INTO Professor (id, nome, sobrenome, dataNascimento, curso) VALUES (4, 'Juliana', 'Ferreira', '1982-07-10', 'DIREITO');
INSERT INTO Professor (id, nome, sobrenome, dataNascimento, curso) VALUES (5, 'Marcelo', 'Santos', '1977-09-28', 'SISTEMA');
INSERT INTO Professor (id, nome, sobrenome, dataNascimento, curso) VALUES (6, 'Patrícia', 'Gonçalves', '1981-11-12', 'DIREITO');
INSERT INTO Professor (id, nome, sobrenome, dataNascimento, curso) VALUES (7, 'Gustavo', 'Rodrigues', '1979-02-08', 'SISTEMA');
INSERT INTO Professor (id, nome, sobrenome, dataNascimento, curso) VALUES (8, 'Camila', 'Alves', '1983-04-17', 'DIREITO');
INSERT INTO Professor (id, nome, sobrenome, dataNascimento, curso) VALUES (9, 'Lucas', 'Pereira', '1976-06-05', 'SISTEMA');
INSERT INTO Professor (id, nome, sobrenome, dataNascimento, curso) VALUES (10, 'Mariana', 'Lima', '1980-08-23', 'DIREITO');
INSERT INTO Professor (id, nome, sobrenome, dataNascimento, curso) VALUES (11, 'Rafael', 'Martins', '1978-10-09', 'SISTEMA');
INSERT INTO Professor (id, nome, sobrenome, dataNascimento, curso) VALUES (12, 'Laura', 'Sousa', '1982-12-27', 'DIREITO');
INSERT INTO Professor (id, nome, sobrenome, dataNascimento, curso) VALUES (13, 'Diego', 'Fernandes', '1977-02-18', 'SISTEMA');
INSERT INTO Professor (id, nome, sobrenome, dataNascimento, curso) VALUES (14, 'Isabela', 'Carvalho', '1981-04-06', 'DIREITO');
INSERT INTO Professor (id, nome, sobrenome, dataNascimento, curso) VALUES (15, 'André', 'Nunes', '1979-06-25', 'SISTEMA');
INSERT INTO Professor (id, nome, sobrenome, dataNascimento, curso) VALUES (16, 'Bruno', 'Gomes', '1983-08-13', 'DIREITO');
INSERT INTO Professor (id, nome, sobrenome, dataNascimento, curso) VALUES (17, 'Carla', 'Costa', '1976-10-31', 'SISTEMA');
INSERT INTO Professor (id, nome, sobrenome, dataNascimento, curso) VALUES (18, 'Renato', 'Mendes', '1980-12-18', 'DIREITO');
INSERT INTO Professor (id, nome, sobrenome, dataNascimento, curso) VALUES (19, 'Hugo', 'Pinto', '1978-02-10', 'SISTEMA');
INSERT INTO Professor (id, nome, sobrenome, dataNascimento, curso) VALUES (20, 'Fernanda', 'Freitas', '1982-04-29', 'DIREITO');
INSERT INTO Professor (id, nome, sobrenome, dataNascimento, curso) VALUES (21, 'Vitor', 'Araújo', '1977-06-16', 'SISTEMA');
INSERT INTO Professor (id, nome, sobrenome, dataNascimento, curso) VALUES (22, 'Ana', 'Campos', '1981-08-03', 'DIREITO');
INSERT INTO Professor (id, nome, sobrenome, dataNascimento, curso) VALUES (23, 'Pedro', 'Siqueira', '1979-09-21', 'SISTEMA');
INSERT INTO Professor (id, nome, sobrenome, dataNascimento, curso) VALUES (24, 'João', 'Barbosa', '1976-01-26', 'SISTEMA');
INSERT INTO Professor (id, nome, sobrenome, dataNascimento, curso) VALUES (25, 'Maria', 'Lopes', '1985-09-10', 'SISTEMA');

INSERT INTO Endereco (id, descricao, logradouro, data) VALUES (1, 'Endereço do João Silva', 'Rua A, 123', '2023-06-04');
INSERT INTO Endereco (id, descricao, logradouro, data) VALUES (2, 'Endereço da Maria Santos', 'Avenida B, 456', '2023-06-04');
INSERT INTO Endereco (id, descricao, logradouro, data) VALUES (3, 'Endereço do Pedro Ferreira', 'Rua C, 789', '2023-06-04');
INSERT INTO Endereco (id, descricao, logradouro, data) VALUES (4, 'Endereço da Ana Oliveira', 'Avenida D, 987', '2023-06-04');
INSERT INTO Endereco (id, descricao, logradouro, data) VALUES (5, 'Endereço do Lucas Rodrigues', 'Rua E, 654', '2023-06-04');
INSERT INTO Endereco (id, descricao, logradouro, data) VALUES (6, 'Endereço da Carla Ribeiro', 'Avenida F, 321', '2023-06-04');
INSERT INTO Endereco (id, descricao, logradouro, data) VALUES (7, 'Endereço do Gustavo Lima', 'Rua G, 159', '2023-06-04');
INSERT INTO Endereco (id, descricao, logradouro, data) VALUES (8, 'Endereço da Fernanda Alves', 'Avenida H, 753', '2023-06-04');
INSERT INTO Endereco (id, descricao, logradouro, data) VALUES (9, 'Endereço do Rafael Pereira', 'Rua I, 246', '2023-06-04');
INSERT INTO Endereco (id, descricao, logradouro, data) VALUES (10, 'Endereço da Patrícia Gomes', 'Avenida J, 864', '2023-06-04');
INSERT INTO Endereco (id, descricao, logradouro, data) VALUES (11, 'Endereço do Marcelo Costa', 'Rua K, 951', '2023-06-04');
INSERT INTO Endereco (id, descricao, logradouro, data) VALUES (12, 'Endereço da Amanda Martins', 'Avenida L, 357', '2023-06-04');
INSERT INTO Endereco (id, descricao, logradouro, data) VALUES (13, 'Endereço do Bruno Sousa', 'Rua M, 864', '2023-06-04');
INSERT INTO Endereco (id, descricao, logradouro, data) VALUES (14, 'Endereço da Laura Carvalho', 'Avenida N, 753', '2023-06-04');
INSERT INTO Endereco (id, descricao, logradouro, data) VALUES (15, 'Endereço do Diego Barbosa', 'Rua O, 246', '2023-06-04');
INSERT INTO Endereco (id, descricao, logradouro, data) VALUES (16, 'Endereço da Camila Lopes', 'Avenida P, 951', '2023-06-04');
INSERT INTO Endereco (id, descricao, logradouro, data) VALUES (17, 'Endereço do Ricardo Fernandes', 'Rua Q, 357', '2023-06-04');
INSERT INTO Endereco (id, descricao, logradouro, data) VALUES (18, 'Endereço da Mariana Cardoso', 'Avenida R, 864', '2023-06-04');
INSERT INTO Endereco (id, descricao, logradouro, data) VALUES (19, 'Endereço do André Nunes', 'Rua S, 753', '2023-06-04');
INSERT INTO Endereco (id, descricao, logradouro, data) VALUES (20, 'Endereço da Juliana Mendes', 'Avenida T, 246', '2023-06-04');
INSERT INTO Endereco (id, descricao, logradouro, data) VALUES (21, 'Endereço do Hugo Pinto', 'Rua U, 951', '2023-06-04');
INSERT INTO Endereco (id, descricao, logradouro, data) VALUES (22, 'Endereço da Carolina Freitas', 'Avenida V, 357', '2023-06-04');
INSERT INTO Endereco (id, descricao, logradouro, data) VALUES (23, 'Endereço do Renato Araújo', 'Rua W, 864', '2023-06-04');
INSERT INTO Endereco (id, descricao, logradouro, data) VALUES (24, 'Endereço do Vitor Siqueira', 'Rua Y, 246', '2023-06-04');
INSERT INTO Endereco (id, descricao, logradouro, data) VALUES (25, 'Endereço da Luana Gonçalves', 'Avenida Z, 951', '2023-06-04');

INSERT INTO Endereco (id, descricao, logradouro, data) VALUES (26, 'Endereço do Professor Fernando Silva', 'Rua A, 123', '2023-05-01');
INSERT INTO Endereco (id, descricao, logradouro, data) VALUES (27, 'Endereço do Professor Carolina Ribeiro', 'Rua B, 456', '2023-05-02');
INSERT INTO Endereco (id, descricao, logradouro, data) VALUES (28, 'Endereço do Professor Ricardo Oliveira', 'Rua C, 789', '2023-05-03');
INSERT INTO Endereco (id, descricao, logradouro, data) VALUES (29, 'Endereço do Professor Juliana Ferreira', 'Rua D, 321', '2023-05-04');
INSERT INTO Endereco (id, descricao, logradouro, data) VALUES (30, 'Endereço do Professor Marcelo Santos', 'Rua E, 654', '2023-05-05');
INSERT INTO Endereco (id, descricao, logradouro, data) VALUES (31, 'Endereço do Professor Patrícia Gonçalves', 'Rua F, 987', '2023-05-06');
INSERT INTO Endereco (id, descricao, logradouro, data) VALUES (32, 'Endereço do Professor Gustavo Rodrigues', 'Rua G, 135', '2023-05-07');
INSERT INTO Endereco (id, descricao, logradouro, data) VALUES (33, 'Endereço do Professor Camila Alves', 'Rua H, 246', '2023-05-08');
INSERT INTO Endereco (id, descricao, logradouro, data) VALUES (34, 'Endereço do Professor Lucas Pereira', 'Rua I, 357', '2023-05-09');
INSERT INTO Endereco (id, descricao, logradouro, data) VALUES (35, 'Endereço do Professor Mariana Lima', 'Rua J, 468', '2023-05-10');
INSERT INTO Endereco (id, descricao, logradouro, data) VALUES (36, 'Endereço do Professor Rafael Martins', 'Rua K, 579', '2023-05-11');
INSERT INTO Endereco (id, descricao, logradouro, data) VALUES (37, 'Endereço do Professor Laura Sousa', 'Rua L, 680', '2023-05-12');
INSERT INTO Endereco (id, descricao, logradouro, data) VALUES (38, 'Endereço do Professor Diego Fernandes', 'Rua M, 791', '2023-05-13');
INSERT INTO Endereco (id, descricao, logradouro, data) VALUES (39, 'Endereço do Professor Isabela Carvalho', 'Rua N, 802', '2023-05-14');
INSERT INTO Endereco (id, descricao, logradouro, data) VALUES (40, 'Endereço do Professor André Nunes', 'Rua O, 913', '2023-05-15');
INSERT INTO Endereco (id, descricao, logradouro, data) VALUES (41, 'Endereço do Professor Bruno Gomes', 'Rua P, 124', '2023-05-16');
INSERT INTO Endereco (id, descricao, logradouro, data) VALUES (42, 'Endereço do Professor Carla Costa', 'Rua Q, 235', '2023-05-17');
INSERT INTO Endereco (id, descricao, logradouro, data) VALUES (43, 'Endereço do Professor Renato Mendes', 'Rua R, 346', '2023-05-18');
INSERT INTO Endereco (id, descricao, logradouro, data) VALUES (44, 'Endereço do Professor Hugo Pinto', 'Rua S, 457', '2023-05-19');
INSERT INTO Endereco (id, descricao, logradouro, data) VALUES (45, 'Endereço do Professor Fernanda Freitas', 'Rua T, 568', '2023-05-20');
INSERT INTO Endereco (id, descricao, logradouro, data) VALUES (46, 'Endereço do Professor Vitor Araújo', 'Rua U, 679', '2023-05-21');
INSERT INTO Endereco (id, descricao, logradouro, data) VALUES (47, 'Endereço do Professor Ana Campos', 'Rua V, 790', '2023-05-22');
INSERT INTO Endereco (id, descricao, logradouro, data) VALUES (48, 'Endereço do Professor Pedro Siqueira', 'Rua W, 801', '2023-05-23');
INSERT INTO Endereco (id, descricao, logradouro, data) VALUES (49, 'Endereço do Professor João Barbosa', 'Rua X, 912', '2023-05-24');
INSERT INTO Endereco (id, descricao, logradouro, data) VALUES (50, 'Endereço do Professor Maria Lopes', 'Rua Y, 123', '2023-06-04');

INSERT INTO Disciplina (id, descricao, curso) VALUES (1, 'Matemática Discreta', 'SISTEMA');
INSERT INTO Disciplina (id, descricao, curso) VALUES (2, 'Algoritmos e Estruturas de Dados', 'SISTEMA');
INSERT INTO Disciplina (id, descricao, curso) VALUES (3, 'Programação Orientada a Objetos', 'SISTEMA');
INSERT INTO Disciplina (id, descricao, curso) VALUES (4, 'Banco de Dados', 'SISTEMA');
INSERT INTO Disciplina (id, descricao, curso) VALUES (5, 'Redes de Computadores', 'SISTEMA');
INSERT INTO Disciplina (id, descricao, curso) VALUES (6, 'Sistemas Operacionais', 'SISTEMA');
INSERT INTO Disciplina (id, descricao, curso) VALUES (7, 'Engenharia de Software', 'SISTEMA');
INSERT INTO Disciplina (id, descricao, curso) VALUES (8, 'Inteligência Artificial', 'SISTEMA');
INSERT INTO Disciplina (id, descricao, curso) VALUES (9, 'Direito Constitucional', 'DIREITO');
INSERT INTO Disciplina (id, descricao, curso) VALUES (10, 'Direito Civil', 'DIREITO');
INSERT INTO Disciplina (id, descricao, curso) VALUES (11, 'Direito Penal', 'DIREITO');
INSERT INTO Disciplina (id, descricao, curso) VALUES (12, 'Direito Administrativo', 'DIREITO');
INSERT INTO Disciplina (id, descricao, curso) VALUES (13, 'Direito do Trabalho', 'DIREITO');
INSERT INTO Disciplina (id, descricao, curso) VALUES (14, 'Direito Tributário', 'DIREITO');
INSERT INTO Disciplina (id, descricao, curso) VALUES (15, 'Direito Internacional', 'DIREITO');
INSERT INTO Disciplina (id, descricao, curso) VALUES (16, 'Teoria Geral do Processo', 'DIREITO');
INSERT INTO Disciplina (id, descricao, curso) VALUES (17, 'Direito Empresarial', 'DIREITO');
INSERT INTO Disciplina (id, descricao, curso) VALUES (18, 'Direito Ambiental', 'DIREITO');
INSERT INTO Disciplina (id, descricao, curso) VALUES (19, 'Direito Penal Empresarial', 'DIREITO');
INSERT INTO Disciplina (id, descricao, curso) VALUES (20, 'Direito Processual Civil', 'DIREITO');
INSERT INTO Disciplina (id, descricao, curso) VALUES (21, 'Direito do Consumidor', 'DIREITO');
INSERT INTO Disciplina (id, descricao, curso) VALUES (22, 'Direito Internacional Privado', 'DIREITO');
INSERT INTO Disciplina (id, descricao, curso) VALUES (23, 'Direito Previdenciário', 'DIREITO');
INSERT INTO Disciplina (id, descricao, curso) VALUES (24, 'Direito de Família', 'DIREITO');
INSERT INTO Disciplina (id, descricao, curso) VALUES (25, 'Direito do Trânsito', 'DIREITO');
