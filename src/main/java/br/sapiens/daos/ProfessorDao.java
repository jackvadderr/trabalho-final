package br.sapiens.daos;

import br.sapiens.configs.ConexaoSingleton;
import br.sapiens.models.CursoEnum;
import br.sapiens.models.DateParse;
import br.sapiens.models.ProfessorModel;

import java.sql.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


public class ProfessorDao implements CrudRepository<ProfessorModel, Integer> {

    private final Connection conn;
    private final String tabela =  "Professor";

    public ProfessorDao() throws SQLException {
        this.conn = new ConexaoSingleton().getConnection();
    }

    @Override
    public <S extends ProfessorModel> long count() throws SQLException {
        String sql = "SELECT COUNT(*) AS total_resultados FROM " + tabela;
        try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
            try (ResultSet rs = pstmt.executeQuery()) {
                if (rs.next()) {
                    return rs.getInt("total_resultados");
                }
            }
        }
        return 0;
    }

    @Override
    public <S extends ProfessorModel> S save(S entity) throws SQLException {
        if (entity.getId() == null)
            return insertInto(entity);
        else
            return update(entity);
    }

    @Override
    public <S extends ProfessorModel> Iterable<ProfessorModel> saveAll(Iterable<S> entities) throws SQLException {
        ArrayList lista = new ArrayList();
        for(S entity : entities) {
            lista.add(save(entity));
        }
        return lista;
    }

    public Iterable<ProfessorModel> findAll() throws SQLException {
        return findAllById(null);
    }

    @Override
    public Optional<ProfessorModel> findById(Integer id) throws SQLException {
        List<ProfessorModel> resultados = (List<ProfessorModel>) findAllById(List.of(id));
        if(resultados == null || resultados.size() != 1) {
            assert resultados != null;
            throw new SQLException("Erro ao buscar valores, não existe somente um resultado! Size "+resultados.size());
        }
        return Optional.ofNullable(resultados.get(0));
    }

    @Override
    public Iterable<ProfessorModel> findAllById(Iterable<Integer> ids) throws SQLException {
        String sql = "select * from " + tabela;
        if(ids != null) {
            List<Integer> lista = new ArrayList();
            Iterator<Integer> interetor = ids.iterator();
            while(interetor.hasNext()){
                lista.add(interetor.next());
            }
            String sqlIN = lista.stream()
                    .map(x -> String.valueOf(x))
                    .collect(Collectors.joining(",", "(", ")"));
            sql = sql+" where id in(?)".replace("(?)", sqlIN);
        }
        PreparedStatement stmt = conn.prepareStatement(sql);
        List<ProfessorModel> resultado = new ArrayList<>();
        try (ResultSet rs = stmt.executeQuery()) {
            while (rs.next()) {
                int id = rs.getInt(1);
                String nome = rs.getString(2);
                String sobrenome = rs.getString(3);
                Date date = rs.getDate(4);
                CursoEnum enumCurso = CursoEnum.valueOf(rs.getString(5));
                ProfessorModel prof = new ProfessorModel(id, nome, sobrenome, date, enumCurso);
                resultado.add(prof);
            }
        }
        return resultado;
    }

    private <S extends ProfessorModel> S update(S entity) throws SQLException {
        String sql = "UPDATE " + tabela + " SET nome = ?, dataNascimento = ?, curso = ?  WHERE id = ?";
        PreparedStatement pstmt = conn.prepareStatement(sql);
        pstmt.setString(1,entity.getNome());
        pstmt.setString(2, new DateParse().parse(entity.getDataNascimento()).toString());
        pstmt.setString(3,entity.getCurso().toString());
        pstmt.setString(4,entity.getId().toString());
        pstmt.executeUpdate();
        return entity;
    }

    private <S extends ProfessorModel> S insertInto(S entity) throws SQLException {
        String sql = "Insert into " + tabela + "(nome, sobrenome, dataNascimento) values(?, ?, ?, ?)";
        PreparedStatement pstmt = conn.prepareStatement(sql,
                Statement.RETURN_GENERATED_KEYS);
        pstmt.setString(1,entity.getNome());
        pstmt.setString(2,entity.getSobrenome());
        pstmt.setString(3, new DateParse().parse(entity.getDataNascimento()).toString());
        pstmt.setString(4,entity.getCurso().toString());
        int affectedRows = pstmt.executeUpdate();
        if (affectedRows == 0)
            throw new SQLException("Falha, nenhuma linha foi inserida");
        ResultSet generatedKeys = pstmt.getGeneratedKeys();
        generatedKeys.next();
        entity.setId(generatedKeys.getInt(1));
        return entity;
    }

    @Override
    public void delete(ProfessorModel entity) throws SQLException {
        if (entity.getId() != null){
            deleteById(entity.getId());
        }
    }

    @Override
    public void deleteById(Integer id) throws SQLException {
        String sql = "DELETE FROM " + tabela + " WHERE id = ?";
        PreparedStatement pstmt = conn.prepareStatement(sql);
        pstmt.setString(1, id.toString());
        pstmt.executeUpdate();
    }

    @Override
    public void deleteAll(Iterable<? extends ProfessorModel> entities) throws SQLException {
        List<Integer> lista = new ArrayList<>();
        Iterator<ProfessorModel> interetor = (Iterator<ProfessorModel>) entities.iterator();
        while(interetor.hasNext()){
            lista.add(interetor.next().getId());
        }
        String sqlIN = lista.stream()
                .map(x -> String.valueOf(x))
                .collect(Collectors.joining(",", "(", ")"));
        String sql = "DELETE FROM " + tabela + " WHERE id in(?)".replace("(?)", sqlIN);
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.executeUpdate();
    }

}
