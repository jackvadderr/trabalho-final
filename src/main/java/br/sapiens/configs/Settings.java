package br.sapiens.configs;

import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.ex.ConfigurationException;

import io.github.cdimascio.dotenv.Dotenv;
public class Settings {

    private final PropertiesConfiguration propertiesConfiguration;

    public Settings() {
        try {
            Configurations configs = new Configurations();
            String applicationProperties = Dotenv.configure().load().get("application.properties");
            propertiesConfiguration = configs.properties(applicationProperties);
        } catch (ConfigurationException e) {
            e.printStackTrace();
            throw new RuntimeException("Falha ao carregar as configurações do arquivo", e);
        }
    }

    public String getUsername() {
        return propertiesConfiguration.getString("database.username");
    }

    public String getPassword() {
        return propertiesConfiguration.getString("database.password");
    }

    public String getDatabaseType() {
        return propertiesConfiguration.getString("database.type").toUpperCase();
    }

    public String getDatabasePort() {
        return propertiesConfiguration.getString("database.port");
    }

}