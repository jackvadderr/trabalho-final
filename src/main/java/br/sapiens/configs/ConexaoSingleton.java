package br.sapiens.configs;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConexaoSingleton {

    private static Connection conn = null;
    private final Settings configuration = new Settings();
    private final String dbUsername = configuration.getUsername();
    private final String dbPassword = configuration.getPassword();
    //private final String databaseType = configuration.getDatabaseType();
    private final String databaseType = "MYSQL";
    private final String databasePort = configuration.getDatabasePort();


    public ConexaoSingleton() throws SQLException {
//        String jdbcURL = "jdbc:h2:mem:test";
//        if(conn == null){
//            conn = DriverManager.getConnection(jdbcURL);
//            System.out.println("Uma conexao está sendo estabelecida");
//        }
    }

    private Connection createMySQLConnection() throws SQLException {
        String jdbcURL = "jdbc:mysql://localhost:3306/gestao_escolar";
        return DriverManager.getConnection(jdbcURL, dbUsername, dbPassword);
    }

    private Connection createPostgreSQLConnection() throws SQLException {
        String jdbcURL = "jdbc:postgresql://localhost:5432/gestao_escolar";
        return DriverManager.getConnection(jdbcURL, dbUsername, dbPassword);
    }

    private static Connection createH2Connection() throws SQLException {
        String jdbcURL = "jdbc:h2:mem:test";
        return DriverManager.getConnection(jdbcURL);
    }

    private static Connection createSQLiteConnection() throws SQLException {
        String jdbcURL = "jdbc:sqlite:/path/to/database.db";
        return DriverManager.getConnection(jdbcURL);
    }

    public Connection getConnection() throws SQLException {
        switch (databaseType) {
            case "MYSQL" -> conn = createMySQLConnection();
            case "POSTGRESQL" -> conn = createPostgreSQLConnection();
            case "H2" -> conn = createH2Connection();
            case "SQLITE" -> conn = createSQLiteConnection();
            default -> throw new IllegalArgumentException("Tipo de banco de dados inválido: " + databaseType);
        }
        return conn;
    }


}