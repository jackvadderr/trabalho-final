package br.sapiens.configs;


import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class CriaEntidades {

    public CriaEntidades(Connection con) throws SQLException {
        String apagarAluno = """
                DROP TABLE IF EXISTS Aluno;
                """;

        String apagarEndereco = """
                DROP TABLE IF EXISTS Endereco;
                """;

        String apagarDisciplina = """
                DROP TABLE IF EXISTS Disciplina;
                """;

        String apagarMatricula = """
                DROP TABLE IF EXISTS Matricula;
                """;

        String endereco = """
                CREATE TABLE IF NOT EXISTS Endereco (
                  id bigint AUTO_INCREMENT,
                  descricao varchar(200),
                  logradouro varchar(200),
                  data date,
                  PRIMARY KEY (id)
                );
                """;

        String disciplinas = """
                CREATE TABLE IF NOT EXISTS Disciplina (
                  id int AUTO_INCREMENT,
                  descricao varchar(200),
                  curso varchar(200),
                  PRIMARY KEY (id)
                );

                """;

        String aluno = """
                CREATE TABLE IF NOT EXISTS Aluno (
                  id int AUTO_INCREMENT,
                  nome varchar(200),
                  sobrenome varchar(200),
                  dataNascimento Date,
                  curso varchar(200),
                  PRIMARY KEY (id)
                );
                """;

        String matricula = """
                CREATE TABLE IF NOT EXISTS Matricula (
                  disciplina int,
                  aluno int,
                  periodo varchar(200),
                  PRIMARY KEY (periodo, disciplina, aluno)
                );
                """;

        String professores = """
            CREATE TABLE IF NOT EXISTS Professor (
              id int AUTO_INCREMENT,
              nome varchar(200),
              sobrenome varchar(200),
              dataNascimento Date,
              PRIMARY KEY(id)
            );
                """;

        Statement statement = con.createStatement();

//        statement.execute(apagarAluno);
//        statement.execute(apagarEndereco);
//        statement.execute(apagarDisciplina);
//        statement.execute(apagarMatricula);

        statement.execute(matricula);
        System.out.println("Tabela matricula criada com sucesso.");
        statement.execute(disciplinas);
        System.out.println("Tabela disciplina criada com sucesso.");
        statement.execute(aluno);
        System.out.println("Tabela aluno criada com sucesso.");
        statement.execute(endereco);
        System.out.println("Tabela endereco criada com sucesso.");
        statement.execute(professores);
        System.out.println("Tabela professores criada com sucesso.");
        System.out.print("Tabelas carregadas com sucesso!\n");
        statement.close();
    }
}
