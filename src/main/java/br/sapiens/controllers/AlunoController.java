package br.sapiens.controllers;

import br.sapiens.daos.AlunoDao;
import br.sapiens.models.AlunoModel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class AlunoController implements Initializable {

    @FXML
    private TableView<AlunoModel> tbData;
    @FXML
    public TableColumn<AlunoModel, Integer> studentId;

    @FXML
    public TableColumn<AlunoModel, String> firstName;

    @FXML
    public TableColumn<AlunoModel, String> lastName;
    @FXML
    private TextField searchField;

    private final AlunoDao alunoDao = new AlunoDao();

    Iterable<AlunoModel> alunosIterable = alunoDao.findAll();
    ObservableList<AlunoModel> alunos = FXCollections.observableArrayList(
            StreamSupport.stream(alunosIterable.spliterator(), false)
                    .collect(Collectors.toList()));

    public AlunoController() throws SQLException {

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        for (AlunoModel registro : alunosIterable) {
            System.out.print("\n" +
                    registro.getId() + " | " +
                    registro.getNome() + " | " +
                    registro.getSobrenome() + " | " +
                    registro.getDataNascimento() + " | " +
                    registro.getCurso());
        }

        studentId.setCellValueFactory(new PropertyValueFactory<>("Id"));
        firstName.setCellValueFactory(new PropertyValueFactory<>("Nome"));
        lastName.setCellValueFactory(new PropertyValueFactory<>("Sobrenome"));
        tbData.setItems(alunos);
    }

    @FXML
    private void searchAlunos() throws SQLException {
        String searchTerm = searchField.getText().toLowerCase();

        if (searchTerm.isEmpty()) {
            loadAlunos();
        } else {
            List<AlunoModel> filteredAlunos = alunos.stream()
                    .filter(aluno -> aluno.getNome().toLowerCase().contains(searchTerm) ||
                            aluno.getSobrenome().toLowerCase().contains(searchTerm))
                    .collect(Collectors.toList());

            tbData.setItems(FXCollections.observableArrayList(filteredAlunos));
        }
    }

    private void loadAlunos() throws SQLException {
        Iterable<AlunoModel> alunosIterable = alunoDao.findAll();
        alunos = FXCollections.observableArrayList(
                StreamSupport.stream(alunosIterable.spliterator(), false)
                        .collect(Collectors.toList()));

        tbData.setItems(alunos);
    }
}