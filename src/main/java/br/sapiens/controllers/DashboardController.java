package br.sapiens.controllers;

import br.sapiens.daos.AlunoDao;
import br.sapiens.models.AlunoModel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class DashboardController implements Initializable {

    @FXML
    private TableView<AlunoModel> tbData;
    @FXML
    public TableColumn<AlunoModel, Integer> studentId;

    @FXML
    public TableColumn<AlunoModel, String> firstName;

    @FXML
    public TableColumn<AlunoModel, String> lastName;
    @FXML
    private PieChart pieChart;

    @FXML
    private Label totalAlunosLabel;

    private final AlunoDao alunoDao = new AlunoDao();

    Iterable<AlunoModel> alunosIterable = alunoDao.findAll();
    ObservableList<AlunoModel> alunos = FXCollections.observableArrayList(
            StreamSupport.stream(alunosIterable.spliterator(), false)
                    .collect(Collectors.toList()));

    public DashboardController() throws SQLException {
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        loadChart();
        loadStudents();
        updateTotalAlunos();
    }

    private void loadChart()
    {

        PieChart.Data slice1 = new PieChart.Data("Classes", 213);
        PieChart.Data slice2 = new PieChart.Data("Attendance"  , 67);
        PieChart.Data slice3 = new PieChart.Data("Teachers" , 36);

        pieChart.getData().add(slice1);
        pieChart.getData().add(slice2);
        pieChart.getData().add(slice3);

    }

    private void loadStudents()
    {
        studentId.setCellValueFactory(new PropertyValueFactory<>("Id"));
        firstName.setCellValueFactory(new PropertyValueFactory<>("Nome"));
        lastName.setCellValueFactory(new PropertyValueFactory<>("Sobrenome"));
        tbData.setItems(alunos);
    }

    private void updateTotalAlunos() {
        try {
            long totalAlunos = alunoDao.count();
            totalAlunosLabel.setText(String.valueOf(totalAlunos));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}