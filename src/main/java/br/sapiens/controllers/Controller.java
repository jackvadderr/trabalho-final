package br.sapiens.controllers;

import br.sapiens.Main;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void btnAluno() throws IOException {
        FXMLLoader fxmlLoader =
                new FXMLLoader(Main.class.getResource("/aluno/lista.fxml"));
        Parent root = fxmlLoader.load();

        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        stage.setTitle("Listagem de alunos");
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.show();
    }

    public void btnDashboard() throws IOException {
        FXMLLoader fxmlLoader =
                new FXMLLoader(Main.class.getResource("/dashboard/dashboard.fxml"));
        Parent root = fxmlLoader.load();

        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        stage.setTitle("Dashboard");
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.show();
    }
    @FXML
    public void btnCadastro() throws IOException {
        System.out.println("\nEntrando na tela de CADASTRO.");
        FXMLLoader fxmlLoader =
                new FXMLLoader(Main.class.getResource("/cadastro/cadastro.fxml"));
        Parent root = fxmlLoader.load();

        Stage stage = new Stage();
        stage.setTitle("Cadastro");
        stage.setScene(new Scene(root));
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.show();
    }

    public void btnConfiguracoes() {
        System.out.println("Entrando na tela de CONFIG.");
    }

    public void btnAtualizacao() {
        System.out.println("Entrando na tela de ATUALIZACAO.");
    }

    public void btnProfessores() throws IOException {
        System.out.println("Entrando na tela de PROFESSORES.");
        FXMLLoader fxmlLoader =
                new FXMLLoader(Main.class.getResource("/professor/lista.fxml"));
        Parent root = fxmlLoader.load();

        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        stage.setTitle("Listagem de alunos");
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.show();
    }
}