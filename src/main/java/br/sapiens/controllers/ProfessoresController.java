package br.sapiens.controllers;

import br.sapiens.daos.ProfessorDao;
import br.sapiens.models.ProfessorModel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class ProfessoresController implements Initializable {

    @FXML
    private TableView<ProfessorModel> tbData;
    @FXML
    public TableColumn<ProfessorModel, Integer> studentId;

    @FXML
    public TableColumn<ProfessorModel, String> firstName;

    @FXML
    public TableColumn<ProfessorModel, String> lastName;
    @FXML
    private TextField searchField;

    private final ProfessorDao professorDao = new ProfessorDao();

    Iterable<ProfessorModel> profIterable = professorDao.findAll();
    ObservableList<ProfessorModel> professores = FXCollections.observableArrayList(
            StreamSupport.stream(profIterable.spliterator(), false)
                    .collect(Collectors.toList()));

    public ProfessoresController() throws SQLException {
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        for (ProfessorModel registro : profIterable) {
            System.out.print("\n" +
                    registro.getId() + " | " +
                    registro.getNome() + " | " +
                    registro.getSobrenome() + " | " +
                    registro.getDataNascimento() + " | " +
                    registro.getCurso());
        }

        studentId.setCellValueFactory(new PropertyValueFactory<>("Id"));
        firstName.setCellValueFactory(new PropertyValueFactory<>("Nome"));
        lastName.setCellValueFactory(new PropertyValueFactory<>("Sobrenome"));
        tbData.setItems(professores);
    }

    @FXML
    private void searchProfessores() throws SQLException {
        String searchTerm = searchField.getText().toLowerCase();

        if (searchTerm.isEmpty()) {
            loadAlunos();
        } else {
            List<ProfessorModel> filteredProf = professores.stream()
                    .filter(professor -> professor.getNome().toLowerCase().contains(searchTerm) ||
                            professor.getSobrenome().toLowerCase().contains(searchTerm))
                    .collect(Collectors.toList());

            tbData.setItems(FXCollections.observableArrayList(filteredProf));
        }
    }

    private void loadAlunos() throws SQLException {
        Iterable<ProfessorModel> alunosIterable = professorDao.findAll();
        professores = FXCollections.observableArrayList(
                StreamSupport.stream(alunosIterable.spliterator(), false)
                        .collect(Collectors.toList()));

        tbData.setItems(professores);
    }
}
