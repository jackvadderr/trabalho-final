package br.sapiens.controllers;

import br.sapiens.Main;
import br.sapiens.daos.AlunoDao;
import br.sapiens.models.AlunoModel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class CadastroController implements Initializable {
    @FXML
    private PieChart pieChart;

    @FXML
    private Label totalAlunosLabel;
    @FXML
    Pane paneTroca;

    private final AlunoDao alunoDao = new AlunoDao();

    Iterable<AlunoModel> alunosIterable = alunoDao.findAll();
    ObservableList<AlunoModel> alunos = FXCollections.observableArrayList(
            StreamSupport.stream(alunosIterable.spliterator(), false)
                    .collect(Collectors.toList()));

    public CadastroController() throws SQLException {
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void cadAluno() throws IOException {
        System.out.println("Cadastrar Aluno");
        FXMLLoader fxmlLoader =
                new FXMLLoader(Main.class.getResource("/cadastro/cadastrar_aluno.fxml"));
        Pane alunoPane = fxmlLoader.load();
        paneTroca.getChildren().clear(); // Limpa os filhos existentes
        paneTroca.getChildren().add(alunoPane);
    }
    public void cadProf() throws IOException {
        System.out.println("Cadastrar Curso");
        FXMLLoader fxmlLoader =
                new FXMLLoader(Main.class.getResource("/cadastro/cadastrar_aluno.fxml"));
        Pane alunoPane = fxmlLoader.load();
        paneTroca.getChildren().clear(); // Limpa os filhos existentes
        paneTroca.getChildren().add(alunoPane);
    }

    public void cadEndereco() throws IOException {
        System.out.println("Cadastrar Endereco");
        FXMLLoader fxmlLoader =
                new FXMLLoader(Main.class.getResource("/cadastro/cadastrar_endereco.fxml"));
        Pane alunoPane = fxmlLoader.load();
        paneTroca.getChildren().clear(); // Limpa os filhos existentes
        paneTroca.getChildren().add(alunoPane);
    }

    public void cadDisciplina() throws IOException {
        System.out.println("Cadastrar Disciplina");
        FXMLLoader fxmlLoader =
                new FXMLLoader(Main.class.getResource("/cadastro/cadastrar_disciplina.fxml"));
        Pane alunoPane = fxmlLoader.load();
        paneTroca.getChildren().clear(); // Limpa os filhos existentes
        paneTroca.getChildren().add(alunoPane);
    }

    public void salvar() {
        System.out.println("Salvando");
    }
}